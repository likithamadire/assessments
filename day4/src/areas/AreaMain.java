package areas;

import areas.Circle;

import areas.Square;

/*import areas.Rectangle;
*/
public class AreaMain {

	public static void main(String[] args) {

		Square square = new Square("square", 2);

		Circle circle = new Circle("circle", 3);

		/*Rectangle rect = new Rectangle("rectangle", 2, 3);
*/
		System.out.println("Square area " + square.calculateArea());

		System.out.println("circle area " + circle.calculateArea());

/*		System.out.println("rectangle area " + rect.calculateArea());
*/
	}

}