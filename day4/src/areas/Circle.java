package areas;

public class Circle extends Shape {

	public final double radius;

	double pi = Math.PI;

	public Circle(String name, double radius) {

		super(name);

		this.radius = radius;

	}

	public double getPi() {

		return pi;

	}

	public void setPi(double pi) {

		this.pi = pi;

	}

	public double getRadius() {

		return radius;

	}

	public float calculateArea() {

		return (float) (pi * (radius * radius));

	}

}