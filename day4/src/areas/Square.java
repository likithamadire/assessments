package areas;

public class Square extends Shape {

	int side;

	public Square(String square, int side) {

		super(square);

		this.side = side;

	}

	public int getSide() {

		return side;

	}

	public void setSide(int side) {

		this.side = side;

	}

	public float calculateArea() {

		return (float) (side * side);

	}

}