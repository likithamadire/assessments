package com.interface1;

import java.util.Scanner;

class MyCalculator implements AdvancedArithmetic {

	int sumOfDivisors = 0;

	public int divisor_sum(int n) {

		for (int i = 1; i <= n; i++) {

			if (n % i == 0) {

				sumOfDivisors += i;

			}

		}

		return (int) sumOfDivisors;

	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("enter input");

		MyCalculator myCalculator = new MyCalculator();

		System.out.println(myCalculator.divisor_sum(sc.nextInt()));

		myCalculator = null;

	}

}