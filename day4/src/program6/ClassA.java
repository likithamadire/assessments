package program6;

public class ClassA {

	static int num = 100;

	public ClassA(int num) {

		super();

		this.num = num;

	}

	/*
	 * 
	 * public static int getNum() { return num; }
	 *
	 *
	 * 
	 * 
	 * 
	 * public static void setNum(int num) { ClassA.num = num; }
	 * 
	 */

	public static void display() {

		System.out.println("num in classA is " + num);

	}

}