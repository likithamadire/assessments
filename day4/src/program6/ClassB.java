package program6;

public class ClassB extends ClassA {

	static int num2 = 43;

	public ClassB(int num, int num2) {

		super(num);

		this.num2 = num2;

	}

	public static void display() {

		System.out.println("num2 in classB is " + num2);

	}

}