package program6;

public class Main {

	public static void main(String[] args) {

		System.out.println("classA num is " + ClassA.num);

		System.out.println("classB num is " + ClassB.num);

		System.out.println("classB num2 is " + ClassB.num2);

		ClassA num3 = new ClassA(90);

		ClassB num4 = new ClassB(25, 46);

		System.out.println(" object num is " + num3.num);

		System.out.println("object2 num is " + num4.num);

		System.out.println("object2 num2 is " + num4.num2);
	}
}