/*Java has built-in mechanism to handle exceptions. 
 * Using the try statement we can test a block of code for errors. 
 * The catch block contains the code that says what to do if exception occurs.
 * This problem will test your knowledge on try-catch block. 
 * You will be given two integers and as input, you have to compute .
 *  If and are not bit signed integers or if is zero,
 *   exception will occur and you have to report it.
 *   Read sample Input/Output to know what to report in case of exceptions.
 *    
 *    @author likhitha.m
 *   
 *    
 *    
 *    Sample Input : 

10 

3 

Sample Output : 

3 

Sample Input : 

10 

Hello 

Sample Output : 

java.util.InputMismatchException 

Sample Input : 

10 

0 

Sample Output : 

java.lang.ArithmeticException: / by zero 

Sample Input : 

23.323 

0 

Sample Output : 

java.util.InputMismatchException 
*/

package com.question2;

import java.util.*;

public class ExceptionTest {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		try {
			System.out.println("enter the number : ");
			int a = new Integer(sc.nextInt());
			System.out.println("enter the number : ");
			int b = new Integer(sc.nextInt());
			try {

				System.out.println("" + (a / b));
			} catch (InputMismatchException e) {
				System.out.println("java.util.InputMismatchException");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		sc.close();

	}
}