package multipleexception;

public class  CustomException  {
	public boolean validateAge(int age) throws InvalidAgeRangeException
	{
	boolean var=false;
	if(age >=19) {
		System.out.println("plater is eligible to participate");
	     var=true;
	}
	else { 
		throw new InvalidAgeRangeException("Invalid Age");
	}
	return var;
}
}