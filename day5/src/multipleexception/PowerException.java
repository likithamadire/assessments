package multipleexception;

import java.util.Scanner;

public class PowerException {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("enter a :");
		int a = sc.nextInt();
		System.out.println("enter b : ");
		int b = sc.nextInt();

		if (a < 0 | b < 0) {
			System.err.println("java.lang.Exception : a or b should not be negative");
		} else if (a == 0 | b == 0) {
			System.err.println("java.lang.Exception :a or b should not be zero");
		} else
			System.err.println("java.lang.Exception : " + Math.pow(a, b));

	}

}
