package com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "Petslist")
@Component
public class Pet {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", length = 5)
	private long slNo;
	@Column(name = "petName")
	@NotEmpty(message = "{petName.required}")
	private String petName;
	@Column(name = "Place")
	@NotEmpty(message = "{place.required}")
	private String place;
	@Column(name = "age")
	@NotBlank
	@Size(max = 3, min = 1)
	@NotEmpty(message = "{age.required}")
	private int age;

	public long getSlNo() {
		return slNo;
	}

	public void setSlNo(long slNo) {
		this.slNo = slNo;
	}

	public String getPetName() {
		return petName;
	}

	public void setPetName(String petName) {
		this.petName = petName;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Pet(long slNo, String petName, String place, int age) {
		super();
		this.slNo = slNo;
		this.petName = petName;
		this.place = place;
		this.age = age;
	}

	public Pet() {
		super();
	}

}
