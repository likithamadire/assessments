package com.dao;

import java.util.List;

import com.entity.User;
import com.service.UserService;

public interface UserDAO {

	public List<User> getUsers();

	public void saveUser(User theUser);

	public User getUser(int userId);

	public void deleteUser(int userId);
	
}
