package com.dao;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.entity.Pet;
import com.entity.User;

@Repository
public class PetDAOImpl implements PetDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Pet> fetchAll() {

		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<User> cq = cb.createQuery(User.class);
		Root<User> root = cq.from(User.class);
		cq.select(root);
		Query query = session.createQuery(cq);
		return query.getResultList();

	}

	@Override
	public void savePet(Pet thePet) {

		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.saveOrUpdate(thePet);
	}

	@Override
	public Pet getPetById(int slNo) {
		Session currentSession = sessionFactory.getCurrentSession();
		Pet pet = currentSession.get(Pet.class, slNo);

		return pet;
	}

	@Override
	public void deletePet(int slNo) {
		Session session = sessionFactory.getCurrentSession();
		Pet book1 = session.byId(Pet.class).load(slNo);
		session.delete(book1);
	}

}
