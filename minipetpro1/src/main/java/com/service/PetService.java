package com.service;

import java.util.List;

import com.entity.Pet;

public interface PetService {

	public List<Pet> fetchAll();

	public void savePet(Pet thePet);

	public Pet getPetById(int slNo);

	public void deletePet(int slNo);

}
