package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.dao.PetDAO;
import com.entity.Pet;

@Service
public class PetServiceImpl implements PetService {

	@Autowired
	private PetDAO PetDAO;

	@Override
	@Transactional
	public List<Pet> fetchAll() {
		return PetDAO.fetchAll();

	}

	@Override
	@Transactional
	public void savePet(Pet thePet) {
		PetDAO.savePet(thePet);

	}

	@Override
	@Transactional
	public Pet getPetById(int slNo) {
		return PetDAO.getPetById(slNo);
	}

	@Override
	@Transactional
	public void deletePet(int slNo) {
		PetDAO.deletePet(slNo);
	}

}
