package com.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.entity.User;


public interface UserService {

	public List<User> getUsers();

	public void saveUser(User theUser);

	public User getUser(int userId);

	public void deleteUser(int userId);
	
	
}
