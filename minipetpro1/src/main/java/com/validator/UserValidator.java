package com.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.entity.User;

@Component
public class UserValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {

		return User.class.equals(arg0);
	}

	@Override
	public void validate(Object arg0, Errors errors) {

		User userreg = (User) arg0;
		if (!(userreg.getPassword().equals(userreg.getConfirmPassword()))) {
			errors.rejectValue("password", "notmatch");

		}
	}

}
