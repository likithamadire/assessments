package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.entity.User;
import com.service.UserService;
import com.validator.UserValidator;

@Controller
@RequestMapping("/User")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserValidator uservalidator;

	@RequestMapping("/register")
	public String showFormForRegister(Model theModel) {
		User theUser = new User();
		theModel.addAttribute("registration", theUser);
		return "Register-form";
	}

	@RequestMapping("/login")
	public String showFormForLogin(Model theModel) {
		User theUser = new User();
		theModel.addAttribute("signin", theUser);
		return "login-user";
	}
	
	@RequestMapping("/loginsuccess")
	public String LoginSuccess(Model theModel) {
		User theUser = new User();
		theModel.addAttribute("success", theUser);
		return "signup-success";
	}

	@RequestMapping("/list")
	public String listuser(Model theModel) {
		List<User> theUsers = userService.getUsers();
		theModel.addAttribute("users", theUsers);
		return "list-customers";
	}

	@RequestMapping("/saveUser")
	public String saveUser(@ModelAttribute("registration") User theUser) {
		userService.saveUser(theUser);
		return "redirect:/User/loginsuccess";
	}

	@RequestMapping("/updateForm")
	public String showFormForUpdate(@RequestParam("userId") int userId, Model theModel) {
		User theUser = userService.getUser(userId);
		theModel.addAttribute("user", theUser);
		return "customer-form";
	}

	@RequestMapping("/delete")
	public String deleteUser(@RequestParam("userId") int userId) {
		userService.deleteUser(userId);
		return "redirect:/User/list";
	}
}
