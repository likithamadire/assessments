package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.entity.Pet;
import com.entity.User;
import com.service.PetService;

@Controller
@RequestMapping("/Pet")
public class PetController {

	@Autowired
	private PetService petService;


	@RequestMapping("/addPet")
	public String showFormForRegister(Model theModel) {
		Pet thepet = new Pet();
		theModel.addAttribute("addpet", thepet);
		return "add-pet";
	}

	@RequestMapping("/petlist")
	public String listuser(Model theModel) {
		List<Pet> thepets = petService.fetchAll();
		theModel.addAttribute("pets", thepets);
		return "list-Pets";
	}

	@RequestMapping("/savePet")
	public String saveUser(@ModelAttribute("addpet")Pet thePet) {
		petService.savePet(thePet);
		return "redirect:/Pet/petlist";
	}

	@RequestMapping("/updatePet")
	public String showFormForUpdate(@RequestParam("slNo") int slNo, Model theModel) {
		Pet thePet = petService.getPetById(slNo);
		theModel.addAttribute("pet", thePet);
		return "redirect:/Pet/petlist";
	}

	@RequestMapping("/deletePet")
	public String deleteUser(@RequestParam("slNo") int slNo) {
		petService.deletePet(slNo);
		return "redirect:/Pet/petlist";
	}
}
