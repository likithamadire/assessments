/*5.	Accept a string, and two indices(integers), and print the substring consisting of all characters inclusive range from ..to . 
Sample Input
Helloworld
3 7
Sample Output
Lowo


*/
package substring;

import java.util.*;

public class SubString {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("enter the statement or string : ");
		String S = in.next();
		System.out.println("enter the index number from where you want to start sub string : ");
		int start = in.nextInt();
		System.out.println("enter the index number from where you want to end sub string : ");
		int end = in.nextInt();

		System.out.println("the final sub string is : " + S.substring(start, end));
		in.close();
	}
}