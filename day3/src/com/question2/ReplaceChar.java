//2.	Write a Java program to replace all the 'd' occurrence characters with �h� characters in each string

package com.question2;

import java.util.Scanner;

public class ReplaceChar {

	public static void main(String[] args) {
	
		Scanner sc= new Scanner(System.in);

		System.out.println("enter a statement ");
		
		String statement = sc.nextLine();
		
		
		String replaceString=statement.replace('d','h');
		
		
		System.out.println(replaceString);
		
		sc.close();
	}

}
