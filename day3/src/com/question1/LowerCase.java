/*Write a Java program to convert all the characters in a string to lowercase. 
 * 
 * @
 * 
 * */


package com.question1;

import java.util.Scanner;

public class LowerCase {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the statement to convert into lower case ");

		String state = sc.nextLine();

		String lowercase = state.toLowerCase();

		System.out.println(lowercase);

		sc.close();

	}

}
