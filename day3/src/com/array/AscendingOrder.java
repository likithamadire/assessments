package com.array;

public class AscendingOrder {    
    public static void main(String[] args) {        
          
        int [] Array = new int [] {1,4,2,6,3,8,2,4,9,2};     
        int temp = 0;    
 
          
       System.out.println("Original array: ");    
       for (int i = 0; i <Array.length; i++) {     
           System.out.print(Array[i] + ",");    
        }    
        
       for (int i = 0; i <Array.length; i++) {     
          for (int j = i+1; j <Array.length; j++) {     
              if(Array[i] >Array[j]) {      
                 temp = Array[i];    
                 Array[i] = Array[j];    
                 Array[j] = temp;    
               }     
            }     
        }    

       System.out.println("\nArray sorted in ascending order: ");    
        for (int i = 0; i <Array.length; i++) {     
            System.out.print(Array[i] + ",");    
        }    
          
    }    
}    