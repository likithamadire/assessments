<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Pet</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600"
	rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<style type="text/css">
#userError, #passError, #passconError {
	font-size: 15px;
	text-align: center;
	color: red;
}

body {
	margin: 0;
	font-size: .9rem;
	font-weight: 400;
	line-height: 1.6;
	color: #212529;
	text-align: left;
	background-color: #f5f8fa;
}

.navbar-laravel {
	box-shadow: 0 2px 4px rgba(0, 0, 0, .04);
}

.navbar-brand, .nav-link, .my-form, .login-form {
	font-family: Raleway, sans-serif;
}

.my-form {
	padding-top: 1.5rem;
	padding-bottom: 1.5rem;
}

.my-form .row {
	margin-left: 0;
	margin-right: 0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-inverse navbar-laravel">
	<div class="container-fluid">
		<ul class="nav navbar-nav">
			<!-- <li class="navbar-brand"><a href="./allpets">Home</a></li> -->
			<li class="navbar-brand"><a class="nav-link" href=#>Pet Peers</a>
		</ul>
		<ul class="nav navbar-nav navbar-right" >
			<li><p style="color: #ffffff" class="navbar-text">
					Welcome,
					<%=(String) request.getSession().getAttribute("username")%></p></li>
			<!-- <li><a href="./userpets">My Pets</a></li>
			<li><a href="./addPet">Add Pet</a></li> -->
			<!-- <li class="nav-item"><a class="nav-link" href="./userpets">My
					Pets</a></li> -->
			<li class="nav-item"><a class="nav-link" href="./allpets">Home</a>
			<li class="nav-item"><a class="nav-link" href="./addPet">Add
					Pet</a>
			<li class="nav-item"><a class="nav-link" href="./logout">Log
					Out</a> <!-- <li><a href="./logout"><span
					class="glyphicon glyphicon-log-in"></span>Logout</a></li> -->
		</ul>
	</div>
	</nav>
	<main class="my-form"> <br></br>
	<div id="addpet">

		<!-- <div class="panel-heading">
			<div class="panel-title">
				<div class="panel-title">
					<h4>
						<b>Pet Information</b>
					</h4>
				</div>
			</div>
		</div> -->
		<div class="row justify-content-center">
			<div class="col-md-5">
				<div class="card">
					<div class="card-header">Pet Information</div>
					<div class="card-body">
						<div style="padding-top: 10px" class="panel-body">
							<form:form action="./savePet" class="form-horizontal"
								method="post" role="form" modelAttribute="petData">
								<div class="form-group row">
									<label for="full_name"
										class="col-md-3 col-form-label text-md-right">Pet Name</label>
									<div class="col-md-6">
										<!-- <label for="Name">Pet Name</label><br> -->
										<form:input id="name" type="text" class="form-control"
											name="petname" path="name" placeholder="Enter pet name"
											size="30" />
									</div>
								</div>
								<div class="form-group row">
									<label for="full_name"
										class="col-md-3 col-form-label text-md-right">Pet Age</label>
									<div class="col-md-6">
										<!-- <label for="NameDemo">Pet Age</label><br> -->
										<form:input id="age" type="text" class="form-control"
											name="petage" path="age" placeholder="Enter pet age"
											size="30" />
									</div>
								</div>
								<div class="form-group row">
									<label for="full_name"
										class="col-md-3 col-form-label text-md-right">Pet
										Place</label>
									<div class="col-md-6">
										<!-- <label for="Place">Pet Place</label><br> -->
										<form:input id="place" type="text" class="form-control"
											name="petplace" path="place" placeholder="Enter pet place"
											size="30" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-0 col-sm-9 m-t-15">
										<button type="submit" class="btn btn-primary">Save</button>
										<button type="reset" class="btn btn-primary">Cancel</button>
									</div>
								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</main>

	<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>
</html>