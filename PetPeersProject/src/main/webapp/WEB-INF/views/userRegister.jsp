<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="dns-prefetch" href="https://fonts.gstatic.com">
 <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<style type="text/css">
#userError, #passError, #passconError {
	font-size: 15px;
	text-align: center;
	color: red;
}
body{
    margin: 0;
    font-size: .9rem;
    font-weight: 400;
    line-height: 1.6;
    color: #212529;
    text-align: left;
    background-color: #f5f8fa;
}
.navbar-laravel
{
    box-shadow: 0 2px 4px rgba(0,0,0,.04);
}
.navbar-brand , .nav-link, .my-form, .login-form
{
    font-family: Raleway, sans-serif;
}
.my-form
{
    padding-top: 1.5rem;
    padding-bottom: 1.5rem;
}
.my-form .row
{
    margin-left: 0;
    margin-right: 0;
}
</style>
<script>
	function usernamecheck() {
		var name1 = document.getElementById("username").value;
		if (name1 == "" || name1 == null) {
			document.getElementById("userError").innerHTML="User should not be empty";
			document.my-form.name1.focus();
			return false;
		}
	}

	function CheckPassword() {
		var password = document.getElementById("pwd").value;
		var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
		if (password ==0 ) {
			document.getElementById("passError").innerHTML ="Password should not empty";
			document.my-form.pwd.focus();
			return false;
		}
		else if(password.length<6 || !passw.test(password)){
			document.getElementById("passError").innerHTML ="Password should have atleast one captial,special character";
			document.my-form.pwd.focus();
			return false;
		}
	}

	function checkpswd() {
		var password = document.getElementById("repwd").value;
		var confirmPassword = document.getElementById("repwd").value;
		if (confirmPassword ==""|| password != confirmPassword) {
			document.getElementById("passconError").innerHTML ="Confirm password should match password";
			document.my-form.repwd.focus();
			return false;
		}
	}
</script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light navbar-laravel">
	<div class="container">
		<a class="navbar-brand" href="#">PetApplication</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link" href="./loginPage">Login</a></li>
				<li class="nav-item"><a class="nav-link" href="./add">Register</a>
				</li>
			</ul>

		</div>
	</div>
	</nav>
	<main class="my-form">
	<div class="cotainer">
		<div class="row justify-content-center">
			<div class="col-md-5">
				<div class="card">
					<div class="card-header">Register</div>
					<div class="card-body">

						<form:form action="user/register" name="my-form" method="post" 
						
							modelAttribute="newUser">
							<div class="form-group row">
								<label for="full_name"
									class="col-md-4 col-form-label text-md-right">Username</label>
								<div class="col-md-6">
									<form:input type="text" class="form-control" path="username"
										id="username" placeholder="Enter your UserName"
										onblur="usernamecheck()" />
									<span id="userError"></span>
								</div>
							</div>
							<div class="form-group row">
								<label for="full_name"
									class="col-md-4 col-form-label text-md-right">Password</label>
								<div class="col-md-6">
									<form:input type="password" class="form-control" id="pwd"
										path="userPassword" placeholder="Enter password" minlength="6"
										onblur="CheckPassword()" />
									<span id="passError"></span>
								</div>
							</div>
							<div class="form-group row">
								<label for="full_name"
									class="col-md-4 col-form-label text-md-right">Confirm
									password</label>
								<div class="col-md-6">
									<form:input type="password" class="form-control" id="repwd"
										path="confirmPassword" placeholder="Re-Enter password"
										minlength="6" onblur="checkpswd()" />
									<span id="passconError"></span>
								</div>
							</div>
							<div class="col-md-6 offset-md-4">
								<button type="submit" class="btn btn-primary">Register
								</button>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
	</main>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>
</html>