package com.hcl.pp.dao;

import java.util.List;

import com.hcl.pp.customException.UserException;
import com.hcl.pp.model.Pet;

public interface PetDao {

	public Pet getPetById(Long id) throws UserException;
	public void savePet(Pet pet);
	public List<Pet> fetchAll() throws UserException;
}
