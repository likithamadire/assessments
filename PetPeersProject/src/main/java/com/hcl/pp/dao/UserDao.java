package com.hcl.pp.dao;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Set;

import com.hcl.pp.customException.UserException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;

public interface UserDao {

	public boolean addUser(User user) throws UserException, SQLIntegrityConstraintViolationException;
	public boolean updateUser(User user);
	public List<User> listUsers() throws UserException;
	public User getUserById(Long id) throws UserException;
	public User findByUserName(String username) throws UserException;
	public boolean removeUser(User user);
	public void authenticateUser();
	public boolean buyPet(Pet pet,Long userId) throws UserException;
	public Set<Pet> getMyPets(String name);
}
