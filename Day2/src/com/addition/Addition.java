package com.addition;

import java.util.Scanner;

public class Addition {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int sum = 0;
		
		String input = "";
		System.out.println("enter the numbers : ");
		int num; 
		
		while ((num = scan.nextInt()) != 0) {
			if (input == "") {

				input = "" + num;
			} else {

				input = input + "+" + num;

			}

			sum = sum + num;

			if (!input.equals("" + num)) {
				System.out.println(input + "=" + sum);

			}
			scan.close();
		}
	}
}
