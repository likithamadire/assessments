
package com.main;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Date1 {

	public static void main(String[] args) {
		Date date = new Date();// deprecated=old version
		System.out.println(date);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");// pattern
		System.out.println(simpleDateFormat.format(date));
		Calendar c = Calendar.getInstance();
		System.out.println("The current Date is:" + c.getTime());

	}

}
